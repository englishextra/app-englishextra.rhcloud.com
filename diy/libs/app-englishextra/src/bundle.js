/*jslint browser: true */
/*jslint node: true */
/*global ActiveXObject, appendFragment, debounce,
earlyDeviceOrientation, earlyDeviceSize, earlyDeviceType,
earlyFnGetYyyymmdd, earlyHasTouch, earlySvgasimgSupport,
earlySvgSupport, findPos, getHTTP, getKeyValuesFromJSON, imagePromise,
insertExternalHTML, isValidId, jQuery, Kamil, loadJS, loadUnparsedJSON,
Masonry, openDeviceBrowser, Packery, parseLink, Promise, QRCode,
removeChildren, require, routie, safelyParseJSON, scriptIsLoaded,
scroll2Top, setStyleDisplayNone, setStyleOpacity, throttle, Timers,
truncString, unescape, verge */
/*property console, split */
/*!
 * define global root
 */
/* var globalRoot = "object" === typeof window && window || "object" === typeof self && self || "object" === typeof global && global || {}; */
var globalRoot = "undefined" !== typeof window ? window : this;
/*!
 * safe way to handle console.log
 * @see {@link https://github.com/paulmillr/console-polyfill}
 */
(function(root){"use strict";if(!root.console){root.console={};}var con=root.console;var prop,method;var dummy=function(){};var properties=["memory"];var methods=("assert,clear,count,debug,dir,dirxml,error,exception,group,"+"groupCollapsed,groupEnd,info,log,markTimeline,profile,profiles,profileEnd,"+"show,table,time,timeEnd,timeline,timelineEnd,timeStamp,trace,warn").split(",");while((prop=properties.pop())){if(!con[prop]){con[prop]={};}}while((method=methods.pop())){if(!con[method]){con[method]=dummy;}}}(globalRoot));
/*!
 * modified verge 1.9.1+201402130803
 * @see {@link https://github.com/ryanve/verge}
 * MIT License 2013 Ryan Van Etten
 * removed module
 * converted to dot notation
 * added &&r.left<=viewportW()&&(0!==el.offsetHeight);
 * added &&r.left<=viewportW()&&(0!==el.offsetHeight);
 * added &&r.top<=viewportH()&&(0!==el.offsetHeight);
 * Substitute inViewport with: inY on vertical sites, inX on horizontal ones.
 * On pages without horizontal scroll, inX is always true.
 * On pages without vertical scroll, inY is always true.
 * If the viewport width is >= the document width, then inX is always true.
 * bug: inViewport returns true if element is hidden
 * @see {@link https://github.com/ryanve/verge/issues/19}
 * @see {@link https://github.com/ryanve/verge/blob/master/verge.js}
 * passes jshint
 */
(function(root){"use strict";var verge=(function(){var xports={},win=typeof root!=="undefined"&&root,doc=typeof document!=="undefined"&&document,docElem=doc&&doc.documentElement,matchMedia=win.matchMedia||win.msMatchMedia,mq=matchMedia?function(q){return!!matchMedia.call(win,q).matches;}:function(){return false;},viewportW=xports.viewportW=function(){var a=docElem.clientWidth,b=win.innerWidth;return a<b?b:a;},viewportH=xports.viewportH=function(){var a=docElem.clientHeight,b=win.innerHeight;return a<b?b:a;};xports.mq=mq;xports.matchMedia=matchMedia?function(){return matchMedia.apply(win,arguments);}:function(){return{};};function viewport(){return{"width":viewportW(),"height":viewportH()};}xports.viewport=viewport;xports.scrollX=function(){return win.pageXOffset||docElem.scrollLeft;};xports.scrollY=function(){return win.pageYOffset||docElem.scrollTop;};function calibrate(coords,cushion){var o={};cushion=+cushion||0;o.width=(o.right=coords.right+cushion)-(o.left=coords.left-cushion);o.height=(o.bottom=coords.bottom+cushion)-(o.top=coords.top-cushion);return o;}function rectangle(el,cushion){el=el&&!el.nodeType?el[0]:el;if(!el||1!==el.nodeType){return false;}return calibrate(el.getBoundingClientRect(),cushion);}xports.rectangle=rectangle;function aspect(o){o=null===o?viewport():1===o.nodeType?rectangle(o):o;var h=o.height,w=o.width;h=typeof h==="function"?h.call(o):h;w=typeof w==="function"?w.call(o):w;return w/h;}xports.aspect=aspect;xports.inX=function(el,cushion){var r=rectangle(el,cushion);return!!r&&r.right>=0&&r.left<=viewportW()&&(0!==el.offsetHeight);};xports.inY=function(el,cushion){var r=rectangle(el,cushion);return!!r&&r.bottom>=0&&r.top<=viewportH()&&(0!==el.offsetHeight);};xports.inViewport=function(el,cushion){var r=rectangle(el,cushion);return!!r&&r.bottom>=0&&r.right>=0&&r.top<=viewportH()&&r.left<=viewportW()&&(0!==el.offsetHeight);};return xports;}());root.verge=verge;}(globalRoot));
/*!
 * return image is loaded promise
 * @see {@link https://jsfiddle.net/englishextra/56pavv7d/}
 * @param {String|Object} s image path string or HTML DOM Image Object
 * var m = document.querySelector("img") || "";
 * var s = m.src || "";
 * imagePromise(m).then(function (r) {
 * alert(r);
 * }).catch (function (err) {
 * alert(err);
 * });
 * imagePromise(s).then(function (r) {
 * alert(r);
 * }).catch (function (err) {
 * alert(err);
 * });
 * @see {@link https://gist.github.com/englishextra/3e95d301d1d47fe6e26e3be198f0675e}
 * passes jshint
 */
(function(root){"use strict";var imagePromise=function(s){if(root.Promise){return new Promise(function(y,n){var f=function(e,p){e.onload=function(){y(p);};e.onerror=function(){n(p);};e.src=p;};if("string"===typeof s){var a=new Image();f(a,s);}else{if("img"!==s.tagName){return Promise.reject();}else{if(s.src){f(s,s.src);}}}});}else{throw new Error("Promise is not in global object");}};(globalRoot).imagePromise=imagePromise;}(globalRoot));
/*!
 * modified scrollToY
 * @see {@link http://stackoverflow.com/questions/8917921/cross-browser-javascript-not-jquery-scroll-to-top-animation}
 * passes jshint
 */
(function(root){"use strict";var scroll2Top=function(scrollTargetY,speed,easing){var scrollY=root.scrollY||document.documentElement.scrollTop;scrollTargetY=scrollTargetY||0;speed=speed||2000;easing=easing||'easeOutSine';var currentTime=0;var time=Math.max(0.1,Math.min(Math.abs(scrollY-scrollTargetY)/speed,0.8));var easingEquations={easeOutSine:function(pos){return Math.sin(pos*(Math.PI/2));},easeInOutSine:function(pos){return(-0.5*(Math.cos(Math.PI*pos)-1));},easeInOutQuint:function(pos){if((pos/=0.5)<1){return 0.5*Math.pow(pos,5);}return 0.5*(Math.pow((pos-2),5)+2);}};function tick(){currentTime+=1/60;var p=currentTime/time;var t=easingEquations[easing](p);if(p<1){requestAnimationFrame(tick);root.scrollTo(0,scrollY+((scrollTargetY-scrollY)*t));}else{root.scrollTo(0,scrollTargetY);}}tick();};root.scroll2Top=scroll2Top;}(globalRoot));
/*!
 * Super lightweight script (~1kb) to detect via Javascript events like
 * 'tap' 'dbltap' "swipeup" "swipedown" "swipeleft" "swiperight"
 * on any kind of device.
 * Version: 2.0.1
 * Author: Gianluca Guarini
 * Contact: gianluca.guarini@gmail.com
 * Website: http://www.gianlucaguarini.com/
 * Twitter: @gianlucaguarini
 * Copyright (c) Gianluca Guarini
 * @see {@link https://github.com/GianlucaGuarini/Tocca.js/blob/master/Tocca.js}
 * passes jshint
 */
(function(doc,win){"use strict";if(typeof doc.createEvent!=="function"){return false;}var pointerEventSupport=function(type){var lo=type.toLowerCase(),ms="MS"+type;return navigator.msPointerEnabled?ms:win.PointerEvent?lo:false;},defaults={useJquery:!win.IGNORE_JQUERY&&typeof jQuery!=="undefined",swipeThreshold:win.SWIPE_THRESHOLD||100,tapThreshold:win.TAP_THRESHOLD||150,dbltapThreshold:win.DBL_TAP_THRESHOLD||200,longtapThreshold:win.LONG_TAP_THRESHOLD||1000,tapPrecision:win.TAP_PRECISION/2||60/2,justTouchEvents:win.JUST_ON_TOUCH_DEVICES},wasTouch=false,touchevents={touchstart:pointerEventSupport("PointerDown")||"touchstart",touchend:pointerEventSupport("PointerUp")||"touchend",touchmove:pointerEventSupport("PointerMove")||"touchmove"},isTheSameFingerId=function(e){return!e.pointerId||typeof pointerId==="undefined"||e.pointerId===pointerId;},setListener=function(elm,events,callback){var eventsArray=events.split(" "),i=eventsArray.length;while(i--){elm.addEventListener(eventsArray[i],callback,false);}},getPointerEvent=function(event){return event.targetTouches?event.targetTouches[0]:event;},getTimestamp=function(){return new Date().getTime();},sendEvent=function(elm,eventName,originalEvent,data){var customEvent=doc.createEvent("Event");customEvent.originalEvent=originalEvent;data=data||{};data.x=currX;data.y=currY;data.distance=data.distance;if(defaults.useJquery){customEvent=jQuery.Event(eventName,{originalEvent:originalEvent});jQuery(elm).trigger(customEvent,data);}if(customEvent.initEvent){for(var key in data){if(data.hasOwnProperty(key)){customEvent[key]=data[key];}}customEvent.initEvent(eventName,true,true);elm.dispatchEvent(customEvent);}while(elm){if(elm["on"+eventName]){elm["on"+eventName](customEvent);}elm=elm.parentNode;}},onTouchStart=function(e){if(!isTheSameFingerId(e)){return;}pointerId=e.pointerId;if(e.type!=="mousedown"){wasTouch=true;}if(e.type==="mousedown"&&wasTouch){return;}var pointer=getPointerEvent(e);cachedX=currX=pointer.pageX;cachedY=currY=pointer.pageY;longtapTimer=setTimeout(function(){sendEvent(e.target,"longtap",e);target=e.target;},defaults.longtapThreshold);timestamp=getTimestamp();tapNum++;},onTouchEnd=function(e){if(!isTheSameFingerId(e)){return;}pointerId=undefined;if(e.type==="mouseup"&&wasTouch){wasTouch=false;return;}var eventsArr=[],now=getTimestamp(),deltaY=cachedY-currY,deltaX=cachedX-currX;clearTimeout(dblTapTimer);clearTimeout(longtapTimer);if(deltaX<=-defaults.swipeThreshold){eventsArr.push("swiperight");}if(deltaX>=defaults.swipeThreshold){eventsArr.push("swipeleft");}if(deltaY<=-defaults.swipeThreshold){eventsArr.push("swipedown");}if(deltaY>=defaults.swipeThreshold){eventsArr.push("swipeup");}if(eventsArr.length){for(var i=0;i<eventsArr.length;i++){var eventName=eventsArr[i];sendEvent(e.target,eventName,e,{distance:{x:Math.abs(deltaX),y:Math.abs(deltaY)}});}tapNum=0;}else{if(cachedX>=currX-defaults.tapPrecision&&cachedX<=currX+defaults.tapPrecision&&cachedY>=currY-defaults.tapPrecision&&cachedY<=currY+defaults.tapPrecision){if(timestamp+defaults.tapThreshold-now>=0){sendEvent(e.target,tapNum>=2&&target===e.target?"dbltap":"tap",e);target=e.target;}}dblTapTimer=setTimeout(function(){tapNum=0;},defaults.dbltapThreshold);}},onTouchMove=function(e){if(!isTheSameFingerId(e)){return;}if(e.type==="mousemove"&&wasTouch){return;}var pointer=getPointerEvent(e);currX=pointer.pageX;currY=pointer.pageY;},tapNum=0,pointerId,currX,currY,cachedX,cachedY,timestamp,target,dblTapTimer,longtapTimer;setListener(doc,touchevents.touchstart+(defaults.justTouchEvents?"":" mousedown"),onTouchStart);setListener(doc,touchevents.touchend+(defaults.justTouchEvents?"":" mouseup"),onTouchEnd);setListener(doc,touchevents.touchmove+(defaults.justTouchEvents?"":" mousemove"),onTouchMove);win.tocca=function(options){for(var opt in options){if(options.hasOwnProperty(opt)){defaults[opt]=options[opt];}}return defaults;};}(document,globalRoot));
/*!
 * modified routie - a tiny hash router - v0.3.2
 * @see {@link https://github.com/jgallen23/routie/blob/master/dist/routie.js}
 * projects.jga.me/routie
 * copyright Greg Allen 2013
 * MIT License
 * "#" => ""
 * "#/" => "/" wont trigger anything? {@link https://github.com/jgallen23/routie/issues/49}
 * "#/home" => "/home"
 * routie({"/contents": function () {},"/feedback": function () {};};
 * routie.navigate("/somepage");
 * in navigate method setImmediate with setTimeout fallback
 * fixed The body of a for in should be wrapped in an if statement to filter unwanted properties from the prototype.
 * @see {@link https://github.com/jgallen23/routie/blob/master/dist/routie.js}
 * passes jshint
 */
(function(root){"use strict";var routie=(function(){var w=root;var routes=[];var map={};var reference="routie";var oldReference=w[reference];var Route=function(path,name){this.name=name;this.path=path;this.keys=[];this.fns=[];this.params={};this.regex=pathToRegexp(this.path,this.keys,false,false);};Route.prototype.addHandler=function(fn){this.fns.push(fn);};Route.prototype.removeHandler=function(fn){for(var i=0,c=this.fns.length;i<c;i++){var f=this.fns[i];if(fn===f){this.fns.splice(i,1);return;}}};Route.prototype.run=function(params){for(var i=0,c=this.fns.length;i<c;i++){this.fns[i].apply(this,params);}};Route.prototype.match=function(path,params){var m=this.regex.exec(path);if(!m){return false;}for(var i=1,len=m.length;i<len;++i){var key=this.keys[i-1];var val=("string"===typeof m[i])?decodeURIComponent(m[i]):m[i];if(key){this.params[key.name]=val;}params.push(val);}return true;};Route.prototype.toURL=function(params){var path=this.path;for(var param in params){if(params.hasOwnProperty(param)){path=path.replace('/:'+param,'/'+params[param]);}}path=path.replace(/\/:.*\?/g,'/').replace(/\?/g,'');if(path.indexOf(':')!==-1){throw new Error('missing parameters for url: '+path);}return path;};var pathToRegexp=function(path,keys,sensitive,strict){if(path instanceof RegExp){return path;}if(path instanceof Array){path='('+path.join('|')+')';}path=path.concat(strict?'':'/?').replace(/\/\(/g,'(?:/').replace(/\+/g,'__plus__').replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?/g,function(_,slash,format,key,capture,optional){keys.push({name:key,optional:!!optional});slash=slash||'';return''+(optional?'':slash)+'(?:'+(optional?slash:'')+(format||'')+(capture||(format&&'([^/.]+?)'||'([^/]+?)'))+')'+(optional||'');}).replace(/([\/.])/g,'\\$1').replace(/__plus__/g,'(.+)').replace(/\*/g,'(.*)');return new RegExp('^'+path+'$',sensitive?'':'i');};var addHandler=function(path,fn){var s=path.split(" ");var name=(s.length===2)?s[0]:null;path=(s.length===2)?s[1]:s[0];if(!map[path]){map[path]=new Route(path,name);routes.push(map[path]);}map[path].addHandler(fn);};var _r=function(path,fn){if(typeof fn==="function"){addHandler(path,fn);_r.reload();}else if(typeof path==="object"){for(var p in path){if(path.hasOwnProperty(p)){addHandler(p,path[p]);}}_r.reload();}else if(typeof fn==="undefined"){_r.navigate(path);}};_r.lookup=function(name,obj){for(var i=0,c=routes.length;i<c;i++){var route=routes[i];if(route.name===name){return route.toURL(obj);}}};_r.remove=function(path,fn){var route=map[path];if(!route){return;}route.removeHandler(fn);};_r.removeAll=function(){map={};routes=[];};_r.navigate=function(path,options){options=options||{};var silent=options.silent||false;if(silent){removeListener();}setTimeout(function(){root.location.hash=path;if(silent){setTimeout(function(){addListener();},1);}},1);if(root.setImmediate){setImmediate(function(){root.location.hash=path;if(silent){setImmediate(function(){addListener();});}});}else{setTimeout(function(){root.location.hash=path;if(silent){setTimeout(function(){addListener();},1);}},1);}};_r.noConflict=function(){w[reference]=oldReference;return _r;};var getHash=function(){return root.location.hash.substring(1);};var checkRoute=function(hash,route){var params=[];if(route.match(hash,params)){route.run(params);return true;}return false;};var hashChanged=_r.reload=function(){var hash=getHash();for(var i=0,c=routes.length;i<c;i++){var route=routes[i];if(checkRoute(hash,route)){return;}}};var addListener=function(){if(w.addEventListener){w.addEventListener("hashchange",hashChanged,false);}else{w.attachEvent("onhashchange",hashChanged);}};var removeListener=function(){if(w.removeEventListener){w.removeEventListener("hashchange",hashChanged);}else{w.detachEvent("onhashchange",hashChanged);}};addListener();return _r;}());root.routie=routie;}(globalRoot));
/*!
 * add js class to html element
 */
(function(classes){"use strict";if(classes){classes.add("js");}}(document.documentElement.classList||""));
/*!
 * modified MediaHack - (c) 2013 Pomke Nohkan MIT LICENCED.
 * @see {@link https://gist.github.com/englishextra/ff8c9dde94abe32a9d7c4a65e0f2ccac}
 * @see {@link https://jsfiddle.net/englishextra/xg7ce8kc/}
 * removed className fallback and additionally
 * returns earlyDeviceOrientation,earlyDeviceSize
 * Add media query classes to DOM nodes
 * @see {@link https://github.com/pomke/mediahack/blob/master/mediahack.js}
 */
(function(root,selectors){"use strict";var orientation,size,f=function(a){var b=a.split(" ");if(selectors){for(var c=0;c<b.length;c+=1){a=b[c];selectors.add(a);}}},g=function(a){var b=a.split(" ");if(selectors){for(var c=0;c<b.length;c+=1){a=b[c];selectors.remove(a);}}},h={landscape:"all and (orientation:landscape)",portrait:"all and (orientation:portrait)"},k={small:"all and (max-width:768px)",medium:"all and (min-width:768px) and (max-width:991px)",large:"all and (min-width:992px)"},d,mM="matchMedia",m="matches",o=function(a,b){var c=function(a){if(a[m]){f(b);orientation=b;}else{g(b);}};c(a);a.addListener(c);},s=function(a,b){var c=function(a){if(a[m]){f(b);size=b;}else{g(b);}};c(a);a.addListener(c);};for(d in h){if(h.hasOwnProperty(d)){o(root[mM](h[d]),d);}}for(d in k){if(k.hasOwnProperty(d)){s(root[mM](k[d]),d);}}root.earlyDeviceOrientation=orientation||"";root.earlyDeviceSize=size||"";}(globalRoot,document.documentElement.classList||""));
/*!
 * add mobile or desktop class
 * using Detect Mobile Browsers | Open source mobile phone detection
 * Regex updated: 1 August 2014
 * detectmobilebrowsers.com
 * @see {@link https://github.com/heikojansen/plack-middleware-detectmobilebrowsers}
 */
(function(root,html,mobile,desktop,opera){"use strict";var selector=(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i).test(opera)||(/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i).test(opera.substr(0,4))?mobile:desktop;if(html){html.classList.add(selector);}root.earlyDeviceType=selector||"";}(globalRoot,document.documentElement||"","mobile","desktop",navigator.userAgent||navigator.vendor||globalRoot.opera));
/*!
 * add svg support class
 */
(function(root,html,selector){"use strict";selector=document.implementation.hasFeature("http://www.w3.org/2000/svg","1.1")?selector:"no-"+selector;if(html){html.classList.add(selector);}root.earlySvgSupport=selector||"";}(globalRoot,document.documentElement||"","svg"));
/*!
 * add svgasimg support class
 */
(function(root,html,selector){"use strict";selector=document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")?selector:"no-"+selector;if(html){html.classList.add(selector);}root.earlySvgasimgSupport=selector||"";}(globalRoot,document.documentElement||"","svgasimg"));
/*!
 * add touch support class
 * @see {@link https://gist.github.com/englishextra/3cb22aab31a52b6760b5921e4fe8db95}
 * @see {@link https://jsfiddle.net/englishextra/z5xhjde8/}
 */
(function(root,html,selector){"use strict";selector="ontouchstart"in html?selector:"no-"+selector;if(html){html.classList.add(selector);}root.earlyHasTouch=selector||"";}(globalRoot,document.documentElement||"","touch"));
/*!
 * return date in YYYY-MM-DD format
 */
(function(root){"use strict";var newDate=(new Date()),newDay=newDate.getDate(),newYear=newDate.getFullYear(),newMonth=newDate.getMonth();(newMonth+=1);if(10>newDay){newDay="0"+newDay;}if(10>newMonth){newMonth="0"+newMonth;}root.earlyFnGetYyyymmdd=newYear+"-"+newMonth+"-"+newDay;}(globalRoot));
/*!
 * append details to title
 */
var initialDocumentTitle = document.title || "",
userBrowsingDetails = " [" + (earlyFnGetYyyymmdd ? earlyFnGetYyyymmdd : "") + (earlyDeviceType ? " " + earlyDeviceType : "") + (earlyDeviceSize ? " " + earlyDeviceSize : "") + (earlyDeviceOrientation ? " " + earlyDeviceOrientation : "") + (earlySvgSupport ? " " + earlySvgSupport : "") + (earlySvgasimgSupport ? " " + earlySvgasimgSupport : "") + (earlyHasTouch ? " " + earlyHasTouch : "") + "]";
if (document.title) {
	document.title = document.title + userBrowsingDetails;
}
/*!
 * Timer management (setInterval / setTimeout)
 * @param {Function} fn
 * @param {Number} ms
 * var timers = new Timers();
 * timers.timeout(function () {
 * console.log("before:", timers);
 * timers.clear();
 * timers = null;
 * doSomething();
 * console.log("after:", timers);
 * }, 3000);
 * @see {@link https://github.com/component/timers}
 * @see {@link https://github.com/component/timers/blob/master/index.js}
 * passes jshint
 */
(function(root){var Timers=function(ids){this.ids=ids||[];};Timers.prototype.timeout=function(fn,ms){var id=setTimeout(fn,ms);this.ids.push(id);return id;};Timers.prototype.interval=function(fn,ms){var id=setInterval(fn,ms);this.ids.push(id);return id;};Timers.prototype.clear=function(){this.ids.forEach(clearTimeout);this.ids=[];};root.Timers=Timers;}(globalRoot));
/*!
 * modified Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * leading edge, instead of the trailing. The function also has a property 'clear'
 * that is a function which will clear the timer to prevent previously scheduled executions.
 * @source underscore.js
 * @see http://unscriptable.com/2009/03/20/debouncing-javascript-methods/
 * @param {Function} function to wrap
 * @param {Number} timeout in ms (`100`)
 * @param {Boolean} whether to execute at the beginning (`false`)
 * @api public
 * @see {@link https://github.com/component/debounce/blob/master/index.js}
 * passes jshint
 */
(function(root,undefined){var debounce=function(func,wait,immediate){var timeout,args,context,timestamp,result;if(undefined===wait||null===wait){wait=100;}function later(){var last=Date.now()-timestamp;if(last<wait&&last>=0){timeout=setTimeout(later,wait-last);}else{timeout=null;if(!immediate){result=func.apply(context,args);context=args=null;}}}var debounced=function(){context=this;args=arguments;timestamp=Date.now();var callNow=immediate&&!timeout;if(!timeout){timeout=setTimeout(later,wait);}if(callNow){result=func.apply(context,args);context=args=null;}return result;};debounced.clear=function(){if(timeout){clearTimeout(timeout);timeout=null;}};debounced.flush=function(){if(timeout){result=func.apply(context,args);context=args=null;clearTimeout(timeout);timeout=null;}};return debounced;};root.debounce=debounce;}(globalRoot));
/*!
 * modified Returns a new function that, when invoked, invokes `func` at most once per `wait` milliseconds.
 * @param {Function} func Function to wrap.
 * @param {Number} wait Number of milliseconds that must elapse between `func` invocations.
 * @return {Function} A new function that wraps the `func` function passed in.
 * @see {@link https://github.com/component/throttle/blob/master/index.js}
 * passes jshint
 */
(function(root,undefined){var throttle=function(func,wait){var ctx,args,rtn,timeoutID;var last=0;return function throttled(){ctx=this;args=arguments;var delta=new Date()-last;if(!timeoutID){if(delta>=wait){call();}else{timeoutID=setTimeout(call,wait-delta);}}return rtn;};function call(){timeoutID=0;last=+new Date();rtn=func.apply(ctx,args);ctx=null;args=null;}};root.throttle=throttle;}(globalRoot));
/*!
 * A simple promise-compatible "document ready" event handler with a few extra treats.
 * With browserify/webpack:
 * const ready = require('document-ready-promise')
 * ready().then(function(){})
 * If in a non-commonjs environment, just include the script. It will attach document.ready for you.
 * document.ready().then(function() {})
 * The document.ready promise will preserve any values that you may be passing through the promise chain.
 * Using ES2015 and fetch
 * fetch(new Request('kitten.jpg'))
 * .then(response => response.blob())
 * .then(document.ready)
 * .then(blob => document.querySelector("img").src = URL.createObjectURL(blob))
 * @see {@link https://github.com/michealparks/document-ready-promise}
 * @see {@link https://github.com/michealparks/document-ready-promise/blob/master/document-ready-promise.js}
 * passes jshint
 */
(function(root){"use strict";var d=root.document;d.ready=function(chainVal){var loaded=(/^loaded|^i|^c/).test(d.readyState),DOMContentLoaded="DOMContentLoaded",load="load";return new Promise(function(resolve){if(loaded){return resolve(chainVal);}function onReady(){resolve(chainVal);d.removeEventListener(DOMContentLoaded,onReady);root.removeEventListener(load,onReady);}d.addEventListener(DOMContentLoaded,onReady);root.addEventListener(load,onReady);});};}(globalRoot));
/*!
 * How can I check if a JS file has been included already?
 * @see {@link https://gist.github.com/englishextra/403a0ca44fc5f495400ed0e20bc51d47}
 * @see {@link https://stackoverflow.com/questions/18155347/how-can-i-check-if-a-js-file-has-been-included-already}
 * @param {String} s path string
 * scriptIsLoaded(s)
 */
(function(root){"use strict";var scriptIsLoaded=function(s){for(var b=document.getElementsByTagName("script")||"",a=0;a<b.length;a+=1){if(b[a].getAttribute("src")===s){return!0;}}return!1;};root.scriptIsLoaded=scriptIsLoaded;}(globalRoot));
/*!
 * Load .json file, but don't JSON.parse it
 * modified JSON with JS.md
 * @see {@link https://gist.github.com/thiagodebastos/08ea551b97892d585f17}
 * @see {@link https://gist.github.com/englishextra/e2752e27761649479f044fd93a602312}
 * @param {String} url path string
 * @param {Object} [callback] callback function
 * @param {Object} [onerror] on error callback function
 * loadUnparsedJSON(url,callback,onerror)
 */
(function(root){"use strict";var loadUnparsedJSON=function(url,callback,onerror){var cb=function(string){return callback&&"function"===typeof callback&&callback(string);},x=root.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.overrideMimeType("application/json;charset=utf-8");x.open("GET",url,!0);x.withCredentials=!1;x.onreadystatechange=function(){if(x.status==="404"||x.status===0){console.log("Error XMLHttpRequest-ing file",x.status);return onerror&&"function"===typeof onerror&&onerror();}else if(x.readyState===4&&x.status===200&&x.responseText){cb(x.responseText);}};x.send(null);};root.loadUnparsedJSON=loadUnparsedJSON;}(globalRoot));
/*!
 * insert External HTML
 * @param {String} id Target Element id
 * @param {String} url path string
 * @param {Object} [callback] callback function
 * @param {Object} [onerror] on error callback function
 * insertExternalHTML(selector,url,callback,onerror)
 */
(function(root){"use strict";var insertExternalHTML=function(id,url,callback,onerror){var d=document,b=d.body||"",gEBI="getElementById",cN="cloneNode",aC="appendChild",pN="parentNode",iH="innerHTML",rC="replaceChild",cR="createRange",cCF="createContextualFragment",cDF="createDocumentFragment",container=d[gEBI](id.replace(/^#/,""))||"",arrange=function(){var x=root.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.overrideMimeType("text/html;charset=utf-8");x.open("GET",url,!0);x.withCredentials=!1;x.onreadystatechange=function(){var cb=function(){return callback&&"function"===typeof callback&&callback();};if(x.status==="404"||x.status===0){console.log("Error XMLHttpRequest-ing file",x.status);return onerror&&"function"===typeof onerror&&onerror();}else if(x.readyState===4&&x.status===200&&x.responseText){var frag=x.responseText;try{var clonedContainer=container[cN](!1);if(d[cR]){var rg=d[cR]();rg.selectNode(b);var df=rg[cCF](frag);clonedContainer[aC](df);return container[pN]?container[pN][rC](clonedContainer,container):container[iH]=frag,cb();}else{clonedContainer[iH]=frag;return container[pN]?container[pN][rC](d[cDF][aC](clonedContainer),container):container[iH]=frag,cb();}}catch(e){console.log(e);}return;}};x.send(null);};if(container){arrange();}};root.insertExternalHTML=insertExternalHTML;}(globalRoot));
/*!
 * parse JSON without try / catch
 * @param {String} a JSON string
 * @see {@link http://stackoverflow.com/questions/11182924/how-to-check-if-javascript-object-is-json}
 * safelyParseJSON(a)
 */
(function(root){"use strict";var safelyParseJSON=function(a){var isJson=function(obj){var t=typeof obj;return['boolean','number',"string",'symbol',"function"].indexOf(t)===-1;};if(!isJson(a)){return JSON.parse(a);}else{return a;}};root.safelyParseJSON=safelyParseJSON;}(globalRoot));
/*!
 * return an array of values that match on a certain key
 * techslides.com/how-to-parse-and-search-json-in-javascript
 * @see {@link https://gist.github.com/englishextra/872269c30d7cb2d10e3c3babdefc37b4}
 * var jpr = JSON.parse(response);
 * for(var i=0,l=jpr.length;i<l;i+=1)
 * {var t=getKeyValuesFromJSON(jpr[i],"label"),
 * p=getKeyValuesFromJSON(jpr[i],"link");};
 * @param {String} b JSON entry
 * @param {String} d JSON key to match
 * getKeyValuesFromJSON(b,d)
 */
(function(root){"use strict";var getKeyValuesFromJSON=function(b,d){var c=[];for(var a in b){if(b.hasOwnProperty(a)){if("object"===typeof b[a]){c=c.concat(getKeyValuesFromJSON(b[a],d));}else{if(a===d){c.push(b[a]);}}}}return c;};root.getKeyValuesFromJSON=getKeyValuesFromJSON;}(globalRoot));
/*!
 * loop over the Array
 * @see {@link https://stackoverflow.com/questions/18238173/javascript-loop-through-json-array}
 * @see {@link https://gist.github.com/englishextra/b4939b3430da4b55d731201460d3decb}
 * @param {String} str any text string
 * @param {Int} max a whole positive number
 * @param {String} add any text string
 * truncString(str,max,add)
 */
(function(root){"use strict";var truncString=function(str,max,add){add=add||"\u2026";return("string"===typeof str&&str.length>max?str.substring(0,max)+add:str);};root.truncString=truncString;}(globalRoot));
/*!
 * remove all children of parent element
 * @see {@link https://gist.github.com/englishextra/da26bf39bc90fd29435e8ae0b409ddc3}
 * @param {Object} e parent HTML Element
 * removeChildren(e)
 */
(function(root){"use strict";var removeChildren=function(e){return (function(){if(e&&e.firstChild){for(;e.firstChild;){e.removeChild(e.firstChild);}}}());};root.removeChildren=removeChildren;}(globalRoot));
/*!
 * append node into other with fragment
 * @see {@link https://gist.github.com/englishextra/0ff3204d5fb285ef058d72f31e3af766}
 * @param {String|object} e an HTML Element to append
 * @param {Object} a target HTML Element
 * appendFragment(e,a)
 */
(function(root){"use strict";var appendFragment=function(e,a){var d=document;a=a||d.getElementsByTagNames("body")[0]||"";return (function(){if(e){var d=document,df=d.createDocumentFragment()||"",aC="appendChild";if("string"===typeof e){e=d.createTextNode(e);}df[aC](e);a[aC](df);}}());};root.appendFragment=appendFragment;}(globalRoot));
/*!
 * set style display block of an element
 * @param {Object} a an HTML Element
 * setStyleDisplayBlock(a)
 */
(function(root){var setStyleDisplayBlock=function(a){return (function(){if(a){a.style.display="block";}}());};root.setStyleDisplayBlock=setStyleDisplayBlock;}(globalRoot));
/*!
 * set style display none of an element
 * @param {Object} a an HTML Element
 * setStyleDisplayNone(a)
 */
(function(root){var setStyleDisplayNone=function(a){return (function(){if(a){a.style.display="none";}}());};root.setStyleDisplayNone=setStyleDisplayNone;}(globalRoot));
/*!
 * set style opacity of an element
 * @param {Object} a an HTML Element
 * @param {Number} n any positive decimal number 0.00-1.00
 * setStyleOpacity(a,n)
 */
(function(root){var setStyleOpacity=function(a,n){n=n||1;return (function(){if(a){a.style.opacity=n;}}());};root.setStyleOpacity=setStyleOpacity;}(globalRoot));
/*!
 * Check if string represents a valid HTML id
 * @see {@link https://gist.github.com/englishextra/b5aaef8b555a3ba84c68a6e251db149d}
 * @see {@link https://jsfiddle.net/englishextra/z19tznau/}
 * @param {String} a text string
 * @param {Int} [full] if true, checks with leading hash/number sign
 * isValidId(a,full)
 */
(function(root){"use strict";var isValidId=function(a,full){return full?/^\#[A-Za-z][-A-Za-z0-9_:.]*$/.test(a)?!0:!1:/^[A-Za-z][-A-Za-z0-9_:.]*$/.test(a)?!0:!1;};root.isValidId=isValidId;}(globalRoot));
/*!
 * find element's position
 * @see {@link https://stackoverflow.com/questions/5598743/finding-elements-position-relative-to-the-document}
 * @param {Object} a an HTML element
 * findPos(a).top
 */
(function(root){"use strict";var findPos=function(a){a=a.getBoundingClientRect();var b=document.body,c=document.documentElement;return{top:Math.round(a.top+(root.pageYOffset||c.scrollTop||b.scrollTop)-(c.clientTop||b.clientTop||0)),left:Math.round(a.left+(root.pageXOffset||c.scrollLeft||b.scrollLeft)-(c.clientLeft||b.clientLeft||0))};};root.findPos=findPos;}(globalRoot));
/*!
 * modified Unified URL parsing API in the browser and node
 * @see {@link https://github.com/wooorm/parse-link}
 * removed module check
 * @see {@link https://gist.github.com/englishextra/4e9a0498772f05fa5d45cfcc0d8be5dd}
 * @see {@link https://gist.github.com/englishextra/2a7fdabd0b23a8433d5fc148fb788455}
 * @see {@link https://jsfiddle.net/englishextra/fcdds4v6/}
 * @param {String} url URL string
 * @param {Boolean} [true|false] if true, returns protocol:, :port, /pathname, ?search, ?query, #hash
 * if set to false, returns protocol, port, pathname, search, query, hash
 * alert(parseLink("http://localhost/search?s=t&v=z#dev").href|
 * origin|host|port|hash|hostname|pathname|protocol|search|query|isAbsolute|isRelative|isCrossDomain);
 */
/*jshint bitwise: false */
(function(root){"use strict";var parseLink=function(url,full){full=full||!1;return (function(){var _r=function(s){return s.replace(/^(#|\?)/,"").replace(/\:$/,"");},l=location||"",_p=function(protocol){switch(protocol){case"http:":return full?":"+80:80;case"https:":return full?":"+443:443;default:return full?":"+l.port:l.port;}},_s=(0===url.indexOf("//")||!!~url.indexOf("://")),w=root.location||"",_o=function(){var o=w.protocol+"//"+w.hostname+(w.port?":"+w.port:"");return o||"";},_c=function(){var c=document.createElement("a");c.href=url;var v=c.protocol+"//"+c.hostname+(c.port?":"+c.port:"");return v!==_o();},a=document.createElement("a");a.href=url;return{href:a.href,origin:_o(),host:a.host||l.host,port:("0"===a.port||""===a.port)?_p(a.protocol):(full?a.port:_r(a.port)),hash:full?a.hash:_r(a.hash),hostname:a.hostname||l.hostname,pathname:a.pathname.charAt(0)!=="/"?(full?"/"+a.pathname:a.pathname):(full?a.pathname:a.pathname.slice(1)),protocol:!a.protocol||":"===a.protocol?(full?l.protocol:_r(l.protocol)):(full?a.protocol:_r(a.protocol)),search:full?a.search:_r(a.search),query:full?a.search:_r(a.search),isAbsolute:_s,isRelative:!_s,isCrossDomain:_c(),hasHTTP:/^(http|https):\/\//i.test(url)?!0:!1};}());};root.parseLink=parseLink;}(globalRoot));
/*jshint bitwise: true */
/*!
 * get current protocol - "http" or "https", else return ""
 * @param {Boolean} [force] When set to "true", and the result is empty,
 * the function will return "http"
 * getHTTP(true)
 */
(function(root){"use strict";var getHTTP=(function(type){return function(force){force=force||"";return"http:"===type?"http":"https:"===type?"https":force?"http":"";};}(root.location.protocol||""));root.getHTTP=getHTTP;}(globalRoot));
/*!
 * Open external links in default browser out of Electron / nwjs
 * @see {@link https://gist.github.com/englishextra/b9a8140e1c1b8aa01772375aeacbf49b}
 * @see {@link https://stackoverflow.com/questions/32402327/how-can-i-force-external-links-from-browser-window-to-open-in-a-default-browser}
 * @see {@link https://github.com/nwjs/nw.js/wiki/shell}
 * electron - file: | nwjs - chrome-extension: | http: Intel XDK
 * wont do in electron and nw,
 * so manageExternalLinkAll will set target blank to links
 * var win = w.open(url, "_blank");
 * win.focus();
 * @param {String} url URL/path string
 * openDeviceBrowser(url)
 * detect Node.js
 * @see {@link https://github.com/lyrictenor/node-is-nwjs/blob/master/is-nodejs.js}
 * @returns {Boolean} true or false
 * detect Electron
 * @returns {Boolean} true or false
 * detect NW.js
 * @see {@link https://github.com/lyrictenor/node-is-nwjs/blob/master/index.js}
 * @returns {Boolean} true or false
 */
(function(root){"use strict";var isNodejs="undefined"!==typeof process&&"undefined"!==typeof require||"",isElectron="undefined"!==typeof root&&root.process&&"renderer"===root.process.type||"",isNwjs=(function(){if("undefined"!==typeof isNodejs&&isNodejs){try{if("undefined"!==typeof require("nw.gui")){return!0;}}catch(e){return!1;}}return!1;}()),openDeviceBrowser=function(url){var triggerForElectron=function(){var es=isElectron?require("electron").shell:"";return es?es.openExternal(url):"";},triggerForNwjs=function(){var ns=isNwjs?require("nw.gui").Shell:"";return ns?ns.openExternal(url):"";},triggerForHTTP=function(){return!0;},triggerForLocal=function(){return root.open(url,"_system","scrollbars=1,location=no");};if(isElectron){triggerForElectron();}else if(isNwjs){triggerForNwjs();}else{var locationProtocol=root.location.protocol||"",hasHTTP=locationProtocol?"http:"===locationProtocol?"http":"https:"===locationProtocol?"https":"":"";if(hasHTTP){triggerForHTTP();}else{triggerForLocal();}}};root.openDeviceBrowser=openDeviceBrowser;}(globalRoot));
/*!
 * loading spinner
 * @requires Timers
 * @see {@link https://gist.github.com/englishextra/24ef040fbda405f7468da70e4f3b69e7}
 * @param {Object} [callback] callback function
 * @param {Int} [delay] any positive whole number, default: 500
 * LoadingSpinner.show();
 * LoadingSpinner.hide(f,n);
 */
var LoadingSpinner = (function () {
	"use strict";
	var d = document,
	b = d.body || "",
	gEBCN = "getElementsByClassName",
	cL = "classList",
	cE = "createElement",
	spinnerClass = "loading-spinner",
	spinner = d[gEBCN](spinnerClass)[0] || "",
	isActiveClass = "is-active-loading-spinner";
	/* console.log("triggered function: LoadingSpinner"); */
	if (!spinner) {
		spinner = d[cE]("div");
		spinner[cL].add(spinnerClass);
		appendFragment(spinner, b);
	}
	return {
		show: function () {
			return b[cL].contains(isActiveClass) || b[cL].add(isActiveClass);
		},
		hide: function (callback, delay) {
			delay = delay || 500;
			var timers = new Timers();
			timers.timeout(function () {
				timers.clear();
				timers = null;
				b[cL].remove(isActiveClass);
				if (callback && "function" === typeof callback) {
					callback();
				}
			}, delay);
		}
	};
}
());
/*!
 * set click event on external links,
 * so that they open in new browser tab
 * @param {Object} [ctx] context HTML Element
 */
var handleExternalLink = function (url, ev) {
	"use strict";
	ev.stopPropagation();
	ev.preventDefault();
	var logicHandleExternalLink = openDeviceBrowser.bind(null, url),
	debounceLogicHandleExternalLink = debounce(logicHandleExternalLink, 200);
	debounceLogicHandleExternalLink();
},
manageExternalLinkAll = function (ctx) {
	"use strict";
	ctx = ctx && ctx.nodeName ? ctx : "";
	var d = document,
	gEBTN = "getElementsByTagName",
	linkTag = "a",
	link = ctx ? ctx[gEBTN](linkTag) || "" : d[gEBTN](linkTag) || "",
	cL = "classList",
	aEL = "addEventListener",
	gA = "getAttribute",
	isBindedClass = "is-binded",
	arrange = function (e) {
		if (!e[cL].contains(isBindedClass)) {
			var url = e[gA]("href") || "";
			if (url && parseLink(url).isCrossDomain && parseLink(url).hasHTTP) {
				e.title = "" + (parseLink(url).hostname || "") + " откроется в новой вкладке";
				if ("undefined" !== typeof getHTTP && getHTTP()) {
					e.target = "_blank";
					e.rel = "noopener";
				} else {
					e[aEL]("click", handleExternalLink.bind(null, url));
				}
				e[cL].add(isBindedClass);
			}
		}
	};
	if (link) {
		/* console.log("triggered function: manageExternalLinkAll"); */
		for (var i = 0, l = link.length; i < l; i += 1) {
			arrange(link[i]);
		}
		/* forEach(link, arrange, false); */
	}
};
document.ready().then(manageExternalLinkAll);
/*!
 * replace img src with data-src
 * initiate on load, not on ready
 * @param {Object} [ctx] context HTML Element
 */
var handleDataSrcImageAll = function () {
	"use strict";
	var d = document,
	gEBCN = "getElementsByClassName",
	cL = "classList",
	ds = "dataset",
	imgClass = "data-src-img",
	img = d[gEBCN](imgClass) || "",
	isActiveClass = "is-active",
	isBindedClass = "is-binded",
	arrange = function (e) {
		/*!
		 * true if elem is in same y-axis as the viewport or within 100px of it
		 * @see {@link https://github.com/ryanve/verge}
		 */
		if (verge.inY(e, 100) /*  && 0 !== e.offsetHeight */) {
			if (!e[cL].contains(isBindedClass)) {
				var srcString = e[ds].src || "";
				if (srcString) {
					if (parseLink(srcString).isAbsolute && !parseLink(srcString).hasHTTP) {
						e[ds].src = srcString.replace(/^/, getHTTP(true) + ":");
						srcString = e[ds].src;
					}
					imagePromise(srcString).then(function (r) {
						e.src = srcString;
					}).catch (function (err) {
						console.log("cannot load image with imagePromise:", srcString);
					});
					e[cL].add(isActiveClass);
					e[cL].add(isBindedClass);
				}
			}
		}
	};
	if (img) {
		/* console.log("triggered function: manageDataSrcImageAll"); */
		for (var i = 0, l = img.length; i < l; i += 1) {
			arrange(img[i]);
		}
		/* forEach(img, arrange, false); */
	}
},
handleDataSrcImageAllWindow = function () {
	var throttleHandleDataSrcImageAll = throttle(handleDataSrcImageAll, 100);
	throttleHandleDataSrcImageAll();
},
manageDataSrcImageAll = function () {
	"use strict";
	var w = globalRoot,
	aEL = "addEventListener",
	rEL = "removeEventListener";
	w[rEL]("scroll", handleDataSrcImageAllWindow, {passive: true});
	w[rEL]("resize", handleDataSrcImageAllWindow);
	w[aEL]("scroll", handleDataSrcImageAllWindow, {passive: true});
	w[aEL]("resize", handleDataSrcImageAllWindow);
	var timers = new Timers();
	timers.timeout(function () {
		timers.clear();
		timers = null;
		handleDataSrcImageAll();
	}, 500);
};
/*!
 * on load, not on ready
 */
globalRoot.addEventListener("load", manageDataSrcImageAll);
/*!
 * init Masonry grid
 * @see {@link https://stackoverflow.com/questions/15160010/jquery-masonry-collapsing-on-initial-page-load-works-fine-after-clicking-home}
 * @see {@link https://gist.github.com/englishextra/5e423ff34f67982f017b}
 * percentPosition: true works well with percent-width items,
 * as items will not transition their position on resize.
 * masonry.desandro.com/options.html
 * use timed out layout property after initialising
 * to level the horizontal gaps
 */
var msnry,
pckry,
initMasonry = function (ctx) {
	"use strict";
	ctx = ctx && ctx.nodeName ? ctx : "";
	var w = globalRoot,
	d = document,
	gEBCN = "getElementsByClassName",
	gridClass = "masonry-grid",
	gridItemClass = "masonry-grid-item",
	gridItemSelector = ".masonry-grid-item",
	gridSizerSelector = ".masonry-grid-sizer",
	grid = ctx ? ctx[gEBCN](gridClass)[0] || "" : d[gEBCN](gridClass)[0] || "",
	gridItem = ctx ? ctx[gEBCN](gridItemClass)[0] || "" : d[gEBCN](gridItemClass)[0] || "",
	initScript = function () {
		if (w.Masonry) {
			/* console.log("function initMasonry.arrangeItems => initialised msnry"); */
			if (msnry) {
				msnry.destroy();
			}
			msnry = new Masonry(grid, {
					itemSelector: gridItemSelector,
					columnWidth: gridSizerSelector,
					gutter: 0,
					percentPosition: true
				});
		} else {
			if (w.Packery) {
				/* console.log("function initMasonry.arrangeItems => initialised pckry"); */
			if (pckry) {
				pckry.destroy();
			}
			pckry = new Packery(grid, {
					itemSelector: gridItemSelector,
					columnWidth: gridSizerSelector,
					gutter: 0,
					percentPosition: true
				});
			}
			var timers = new Timers();
			timers.timeout(function () {
				timers.clear();
				timers = null;
				if ("undefined" !== typeof msnry && msnry) {
					msnry.layout();
				} else {
					if ("undefined" !== typeof pckry && pckry) {
						pckry.layout();
					}
				}
			}, 500);
		}
	};
	if (grid && gridItem) {
		/* console.log("triggered function: initMasonryGrid"); */
		/* var jsUrl = "./cdn/masonry/4.1.1/js/masonry.pkgd.fixed.min.js"; */
		var jsUrl = "./cdn/packery/2.1.1/js/packery.pkgd.fixed.min.js";
		if (!scriptIsLoaded(jsUrl)) {
			loadJS(jsUrl, initScript);
		} else {
			initScript();
		}
	}
};
/*!
 * load json and tronsform into select options
 * add smooth scroll or redirection to static select options
 * @param {Object} [ctx] context HTML Element
 */
var handleContentsSelect = function () {
	"use strict";
	var _this = this;
	var w = window,
	d = document,
	gEBI = "getElementById",
	hashString = _this.options[_this.selectedIndex].value || "";
	if (hashString) {
		var tragetObject = isValidId(hashString, true) ? d[gEBI](hashString.replace(/^#/, "")) || "" : "";
		if (tragetObject) {
			scroll2Top(findPos(tragetObject).top, 10000);
		} else {
			w.location.href = hashString;
		}
	}
},
manageContentsSelect = function () {
	"use strict";
	var d = document,
	gEBI = "getElementById",
	cE = "createElement",
	cTN = "createTextNode",
	aC = "appendChild",
	aEL = "addEventListener",
	contentsSelect = d[gEBI]("contents-select") || "",
	jsonUrl = "./libs/app-englishextra/json/contents.json",
	processJsonResponse = function (jsonResponse) {
		var jsonObj;
		try {
			jsonObj = safelyParseJSON(jsonResponse);
			if (!jsonObj[0].label) {
				throw new Error("incomplete JSON data: no label");
			} else {
				if (!jsonObj[0].link) {
					throw new Error("incomplete JSON data: no link");
				}
			}
		} catch (err) {
			console.log("cannot init processJsonResponse", err);
			return;
		}
		var df = d.createDocumentFragment(),
		generateContentsOptions = function (e) {
			var label = getKeyValuesFromJSON(e, "label") || "",
			link = getKeyValuesFromJSON(e, "link") || "";
			if (label && link) {
				var contentsOption = d[cE]("option");
				contentsOption.value = link;
				contentsOption.title = "" + label;
				contentsOption[aC](d[cTN](truncString("" + label, 33)));
				df[aC](contentsOption);
			}
		};
		for (var i = 0, l = jsonObj.length; i < l; i += 1) {
			generateContentsOptions(jsonObj[i]);
		}
		/* forEach(jsonObj, generateContentsOptions, false); */
		contentsSelect[aC](df);
		contentsSelect[aEL]("change", handleContentsSelect);
	};
	if (contentsSelect) {
		loadUnparsedJSON(jsonUrl, processJsonResponse);
	}
};
/*!
 * add smooth scroll or redirection to static select options
 * @param {Object} [ctx] context HTML Element
 */
var handleChaptersSelect = function () {
	"use strict";
	var _this = this;
	var w = globalRoot,
	d = document,
	gEBI = "getElementById",
	hashString = _this.options[_this.selectedIndex].value || "";
	if (hashString) {
		var tragetObject = hashString ? (isValidId(hashString, true) ? d[gEBI](hashString.replace(/^#/,"")) || "" : "") : "";
		if (tragetObject) {
			scroll2Top(findPos(tragetObject).top, 20000);
		} else {
			w.location.href = hashString;
		}
	}
},
manageChaptersSelect = function () {
	"use strict";
	var d = document,
	gEBI = "getElementById",
	aEL = "addEventListener",
	chaptersSelect = d[gEBI]("chapters-select") || "";
	if (chaptersSelect) {
		/* console.log("triggered function: manageChaptersSelect"); */
		chaptersSelect[aEL]("change", handleChaptersSelect);
	}
};
/*!
 * manage search input
 */
var manageSearchInput = function () {
	"use strict";
	var d = document,
	gEBI = "getElementById",
	aEL = "addEventListener",
	searchInput = d[gEBI]("text") || "",
	handleSearchInputValue = function () {
		var _this = this;
		var logicHandleSearchInputValue = function () {
			_this.value = _this.value.replace(/\\/g, "").replace(/ +(?= )/g, " ").replace(/\/+(?=\/)/g, "/") || "";
		},
		debounceLogicHandleSearchInputValue = debounce(logicHandleSearchInputValue, 200);
		debounceLogicHandleSearchInputValue();
	};
	if (searchInput) {
		/* console.log("triggered function: manageSearchInput"); */
		searchInput.focus();
		searchInput[aEL]("input", handleSearchInputValue);
	}
};
document.ready().then(manageSearchInput);
/*!
 * add click event on hidden-layer show btn
 * @param {Object} [ctx] context HTML Element
 */
var handleExpandingLayerAll = function () {
	"use strict";
	var _this = this;
	var cL = "classList",
	pN = "parentNode",
	isActiveClass = "is-active",
	layer = _this[pN] ? _this[pN].nextElementSibling : "";
	if (layer) {
		_this[cL].toggle(isActiveClass);
		layer[cL].toggle(isActiveClass);
	}
	return;
},
manageExpandingLayers = function (ctx) {
	"use strict";
	ctx = ctx && ctx.nodeName ? ctx : "";
	var d = document,
	gEBCN = "getElementsByClassName",
	aEL = "addEventListener",
	btnClass = "btn-expand-hidden-layer",
	btn = ctx ? ctx[gEBCN](btnClass) || "" : d[gEBCN](btnClass) || "",
	addHandler = function (e) {
		e[aEL]("click", handleExpandingLayerAll);
	};
	if (btn) {
		/* console.log("triggered function: manageExpandingLayers"); */
		for (var i = 0, l = btn.length; i < l; i += 1) {
			addHandler(btn[i]);
		}
		/* forEach(btn, addHandler, false); */
	}
};
/*!
 * init qr-code
 * @see {@link https://stackoverflow.com/questions/12777622/how-to-use-enquire-js}
 */
var manageLocationQrCodeImage = function () {
	"use strict";
	var w = globalRoot,
	d = document,
	gEBCN = "getElementsByClassName",
	cL = "classList",
	cE = "createElement",
	aEL = "addEventListener",
	holder = d[gEBCN]("holder-location-qr-code")[0] || "",
	locationHref = w.location.href || "",
	initScript = function () {
		var generateLocationQrCodeImg = function () {
			var locationHref = w.location.href || "",
			img = d[cE]("img"),
			imgTitle = d.title ? ("Ссылка на страницу «" + d.title.replace(/\[[^\]]*?\]/g, "").trim() + "»") : "",
			imgSrc = getHTTP(true) + "://chart.googleapis.com/chart?cht=qr&chld=M%7C4&choe=UTF-8&chs=300x300&chl=" + encodeURIComponent(locationHref);
			img.alt = imgTitle;
			if (w.QRCode) {
				if ("undefined" !== typeof earlySvgSupport && "svg" === earlySvgSupport) {
					imgSrc = QRCode.generateSVG(locationHref, {
							ecclevel: "M",
							fillcolor: "#FFFFFF",
							textcolor: "#191919",
							margin: 4,
							modulesize: 8
						});
					var XMLS = new XMLSerializer();
					imgSrc = XMLS.serializeToString(imgSrc);
					imgSrc = "data:image/svg+xml;base64," + w.btoa(unescape(encodeURIComponent(imgSrc)));
					img.src = imgSrc;
				} else {
					imgSrc = QRCode.generatePNG(locationHref, {
							ecclevel: "M",
							format: "html",
							fillcolor: "#FFFFFF",
							textcolor: "#191919",
							margin: 4,
							modulesize: 8
						});
					img.src = imgSrc;
				}
			} else {
				img.src = imgSrc;
			}
			img[cL].add("qr-code-img");
			img.title = imgTitle;
			removeChildren(holder);
			appendFragment(img, holder);
		};
		generateLocationQrCodeImg();
		w[aEL]("hashchange", generateLocationQrCodeImg);
	};
	if (holder && locationHref) {
		if ("undefined" !== typeof getHTTP && getHTTP()) {
			/* console.log("triggered function: manageLocationQrCodeImage"); */
			var jsUrl = "./cdn/qrjs2/0.1.3/js/qrjs2.fixed.min.js";
			if (!scriptIsLoaded(jsUrl)) {
				loadJS(jsUrl, initScript);
			}
		}
	}
};
document.ready().then(manageLocationQrCodeImage);
/*!
 * init nav-menu
 */
var initNavMenu = function () {
	"use strict";
	var w = globalRoot,
	d = document,
	gEBI = "getElementById",
	gEBCN = "getElementsByClassName",
	gEBTN = "getElementsByTagName",
	cL = "classList",
	aEL = "addEventListener",
	container = d[gEBI]("container") || "",
	page = d[gEBI]("page") || "",
	btnNavMenu = d[gEBCN]("btn-nav-menu")[0] || "",
	panelNavMenu = d[gEBCN]("panel-nav-menu")[0] || "",
	panelNavMenuItems = panelNavMenu ? panelNavMenu[gEBTN]("a") || "" : "",
	holderPanelMenuMore = d[gEBCN]("holder-panel-menu-more")[0] || "",
	isActiveClass = "is-active",
	locationHref = w.location.href || "",
	removeAllActiveClass = function () {
		page[cL].remove(isActiveClass);
		panelNavMenu[cL].remove(isActiveClass);
		btnNavMenu[cL].remove(isActiveClass);
	},
	removeHolderActiveClass = function () {
		if (holderPanelMenuMore && holderPanelMenuMore[cL].contains(isActiveClass)) {
			holderPanelMenuMore[cL].remove(isActiveClass);
		}
	},
	addContainerHandler = function () {
		var handleContainerLeft = function () {
			/* console.log("swipeleft"); */
			removeHolderActiveClass();
			if (panelNavMenu[cL].contains(isActiveClass)) {
				removeAllActiveClass();
			}
		},
		handleContainerRight = function () {
			/* console.log("swiperight"); */
			removeHolderActiveClass();
			var addAllActiveClass = function () {
				page[cL].add(isActiveClass);
				panelNavMenu[cL].add(isActiveClass);
				btnNavMenu[cL].add(isActiveClass);
			};
			if (!panelNavMenu[cL].contains(isActiveClass)) {
				addAllActiveClass();
			}
		};
		container[aEL]("click", handleContainerLeft);
		if (w.tocca) {
			if ("undefined" !== typeof earlyHasTouch && "touch" === earlyHasTouch) {
				container[aEL]("swipeleft", handleContainerLeft);
				container[aEL]("swiperight", handleContainerRight);
			}
		}
	},
	addBtnHandler = function () {
		var toggleAllActiveClass = function () {
			page[cL].toggle(isActiveClass);
			panelNavMenu[cL].toggle(isActiveClass);
			btnNavMenu[cL].toggle(isActiveClass);
		},
		handleBtnNavMenu = function (ev) {
			ev.stopPropagation();
			ev.preventDefault();
			removeHolderActiveClass();
			toggleAllActiveClass();
		};
		btnNavMenu[aEL]("click", handleBtnNavMenu);
	},
	addItemHandlerAll = function () {
		var addItemHandler = function (e) {
			var addActiveClass = function (e) {
				e[cL].add(isActiveClass);
			},
			removeHolderAndAllActiveClass = function () {
				removeHolderActiveClass();
				removeAllActiveClass();
			},
			removeActiveClass = function (e) {
				e[cL].remove(isActiveClass);
			},
			handleItem = function () {
				if (panelNavMenu[cL].contains(isActiveClass)) {
					removeHolderAndAllActiveClass();
				}
				for (var j = 0, l = panelNavMenuItems.length; j < l; j += 1) {
					removeActiveClass(panelNavMenuItems[j]);
				}
				/* forEach(panelNavMenuItems, removeActiveClass, false); */
				addActiveClass(e);
			};
			e[aEL]("click", handleItem);
			if (locationHref === e.href) {
				addActiveClass(e);
			} else {
				removeActiveClass(e);
			}
		};
		for (var i = 0, l = panelNavMenuItems.length; i < l; i += 1) {
			addItemHandler(panelNavMenuItems[i]);
		}
		/* forEach(panelNavMenuItems, addItemHandler, false); */
	};
	if (page && container && btnNavMenu && panelNavMenu && panelNavMenuItems) {
		/* console.log("triggered function: initNavMenu"); */
		/*!
		 * close nav on outside click
		 */
		addContainerHandler();
		/*!
		 * open or close nav
		 */
		addBtnHandler();
		/*!
		 * close nav, scroll to top, highlight active nav item
		 */
		addItemHandlerAll();
	}
};
document.ready().then(initNavMenu);
/*!
 * highlight current nav-menu item
 */
var highlightNavMenuItem = function () {
	"use strict";
	var w = globalRoot,
	d = document,
	gEBCN = "getElementsByClassName",
	gEBTN = "getElementsByTagName",
	cL = "classList",
	panelNavMenu = d[gEBCN]("panel-nav-menu")[0] || "",
	panelNavMenuItems = panelNavMenu ? panelNavMenu[gEBTN]("a") || "" : "",
	isActiveClass = "is-active",
	locationHref = w.location.href || "",
	toggleActiveClass = function (e) {
		if (locationHref === e.href) {
			e[cL].add(isActiveClass);
		} else {
			e[cL].remove(isActiveClass);
		}
	};
	if (panelNavMenu && panelNavMenuItems && locationHref) {
		/* console.log("triggered function: highlightNavMenuItem"); */
		for (var i = 0, l = panelNavMenuItems.length; i < l; i += 1) {
			toggleActiveClass(panelNavMenuItems[i]);
		}
		/* forEach(panelNavMenuItems, toggleActiveClass, false); */
	}
};
globalRoot.addEventListener("hashchange", highlightNavMenuItem);
/*!
 * init menu-more
 */
var initMenuMore = function () {
	"use strict";
	var d = document,
	gEBI = "getElementById",
	gEBCN = "getElementsByClassName",
	gEBTN = "getElementsByTagName",
	cL = "classList",
	aEL = "addEventListener",
	container = d[gEBI]("container") || "",
	page = d[gEBI]("page") || "",
	holderPanelMenuMore = d[gEBCN]("holder-panel-menu-more")[0] || "",
	btnMenuMore = d[gEBCN]("btn-menu-more")[0] || "",
	panelMenuMore = d[gEBCN]("panel-menu-more")[0] || "",
	panelMenuMoreItems = panelMenuMore ? panelMenuMore[gEBTN]("li") || "" : "",
	panelNavMenu = d[gEBCN]("panel-nav-menu")[0] || "",
	isActiveClass = "is-active",
	handleItem = function () {
		page[cL].remove(isActiveClass);
		holderPanelMenuMore[cL].remove(isActiveClass);
		if (panelNavMenu && panelNavMenu[cL].contains(isActiveClass)) {
			panelNavMenu[cL].remove(isActiveClass);
		}
	},
	addContainerHandler = function () {
		container[aEL]("click", handleItem);
	},
	addBtnHandler = function () {
		var h_btn = function (ev) {
			ev.stopPropagation();
			ev.preventDefault();
			holderPanelMenuMore[cL].toggle(isActiveClass);
		};
		btnMenuMore[aEL]("click", h_btn);
	},
	addItemHandlerAll = function () {
		var addItemHandler = function (e) {
			e[aEL]("click", handleItem);
		};
		for (var i = 0, l = panelMenuMoreItems.length; i < l; i += 1) {
			addItemHandler(panelMenuMoreItems[i]);
		}
		/* forEach(panelMenuMoreItems, addItemHandler, false); */
	};
	if (page && container && holderPanelMenuMore && btnMenuMore && panelMenuMore && panelMenuMoreItems) {
		/* console.log("triggered function: initMenuMore"); */
		/*!
		 * hide menu more on outside click
		 */
		addContainerHandler();
		/*!
		 * show or hide menu more
		 */
		addBtnHandler();
		/*!
		 * hide menu more on item clicked
		 */
		addItemHandlerAll();
	}
};
document.ready().then(initMenuMore);
/*!
 * init Pages Kamil autocomplete
 * @see {@link https://github.com/oss6/kamil/wiki/Example-with-label:link-json-and-typo-correct-suggestion}
 */
var initKamilAutocomplete = function () {
	"use strict";
	var w = globalRoot,
	d = document,
	gEBI = "getElementById",
	gEBCN = "getElementsByClassName",
	gEBTN = "getElementsByTagName",
	cTN = "createTextNode",
	searchForm = d[gEBCN]("search-form")[0] || "",
	textInputSelector = "#text",
	textInput = d[gEBI]("text") || "",
	jsonUrl = "./libs/app-englishextra/json/contents.json",
	processJsonResponse = function (jsonResponse) {
		var ac;
		try {
			var jsonObj = safelyParseJSON(jsonResponse);
			if (!jsonObj[0].hasOwnProperty("label")) {
				throw new Error("incomplete JSON data: no label");
			}
			ac = new Kamil(textInputSelector, {
					source: jsonObj,
					property: "label",
					minChars: 2
				});
		} catch (err) {
			console.log("cannot init generateMenu", err);
			return;
		}
		/*!
		 * show suggestions
		 */
		ac.renderMenu = function (ul, items) {
			items = items || "";
			var itemsLength = items.length,
			_this = this,
			/*!
			 * limit output
			 */
			limitKamilOutput = function (e, i) {
				if (i < 10) {
					_this._renderItemData(ul, e, i);
				}
			};
			if (items) {
				for (var i = 0; i < itemsLength; i += 1) {
					limitKamilOutput(items[i], i);
				}
				/* forEach(items, function (e, i) {
					limitKamilOutput(e, i);
				}, false); */
			}
			/*!
			 * truncate text
			 */
			var lis = ul ? ul[gEBTN]("li") || "" : "",
			truncateKamilText = function (e) {
				var truncText = e.firstChild.textContent || "",
				truncTextObj = d[cTN](truncString(truncText, 24));
				e.replaceChild(truncTextObj, e.firstChild);
				/* e.title = "" + truncText; */
			};
			if (lis) {
				for (var j = 0, m = lis.length; j < m; j += 1) {
					truncateKamilText(lis[j]);
				}
				/* forEach(lis, truncateKamilText, false); */
			}
		};
		/*!
		 * unless you specify property option in new Kamil
		 * use kamil built-in word label as search key in JSON file
		 * [{"link":"/","label":"some text to match"},
		 * {"link":"/pages/contents.html","label":"some text to match"}]
		 */
		ac.on("kamilselect", function (e) {
			var kamilItemLink = e.item.link || "",
			handleKamilItem = function () {
				e.inputElement.value = "";
				w.location.href = kamilItemLink;
			};
			if (kamilItemLink) {
				/*!
				 * nwjs wont like setImmediate here
				 */
				/* setImmediate(handleKamilItem); */
				handleKamilItem();
			}
		});
	},
	initScript = function () {
		loadUnparsedJSON(jsonUrl, processJsonResponse);
	};
	if (searchForm && textInput) {
		/* console.log("triggered function: initKamilAutocomplete"); */
		var jsUrl = "./cdn/kamil/0.1.1/js/kamil.fixed.min.js";
		if (!scriptIsLoaded(jsUrl)) {
			loadJS(jsUrl, initScript);
		}
	}
};
document.ready().then(initKamilAutocomplete);
/*!
 * init ui-totop
 */
var initUiTotop = function () {
	"use strict";
	var w = globalRoot,
	d = document,
	h = d.documentElement || "",
	b = d.body || "",
	gEBCN = "getElementsByClassName",
	cL = "classList",
	cE = "createElement",
	aC = "appendChild",
	/* cENS = "createElementNS",
	sANS = "setAttributeNS", */
	aEL = "addEventListener",
	btnClass = "ui-totop",
	btnTitle = "Наверх",
	isActiveClass = "is-active",
	anchor = d[cE]("a"),
	/* insertUpSvg = function (targetObj) {
		var svg = d[cENS]("http://www.w3.org/2000/svg", "svg"),
		use = d[cENS]("http://www.w3.org/2000/svg", "use");
		svg[cL].add("ui-icon");
		use[sANS]("http://www.w3.org/1999/xlink", "xlink:href", "#ui-icon-Up");
		svg[aC](use);
		targetObj[aC](svg);
	}, */
	handleUiTotopAnchor = function (ev) {
		ev.stopPropagation();
		ev.preventDefault();
		scroll2Top(0, 20000);
	},
	handleUiTotopWindow = function (_this) {
		var logicHandleUiTotopWindow = function () {
			var btn = d[gEBCN](btnClass)[0] || "",
			scrollPosition = _this.pageYOffset || h.scrollTop || b.scrollTop || "",
			windowHeight = _this.innerHeight || h.clientHeight || b.clientHeight || "";
			if (scrollPosition && windowHeight && btn) {
				if (scrollPosition > windowHeight) {
					btn[cL].add(isActiveClass);
				} else {
					btn[cL].remove(isActiveClass);
				}
			}
		},
		throttleLogicHandleUiTotopWindow = throttle(logicHandleUiTotopWindow, 100);
		throttleLogicHandleUiTotopWindow();
	};
	anchor[cL].add(btnClass);
	/*jshint -W107 */
	anchor.href = "javascript:void(0);";
	/*jshint +W107 */
	anchor.title = btnTitle;
	/* insertUpSvg(anchor); */
	b[aC](anchor);
	if (b) {
		/* console.log("triggered function: initUiTotop"); */
		anchor[aEL]("click", handleUiTotopAnchor);
		w[aEL]("scroll", handleUiTotopWindow, {passive: true});
	}
};
document.ready().then(initUiTotop);
/*!
 * init routie
 * @param {String} ctx HTML id string
 */
var initRoutie = function () {
	"use strict";
	var d = document,
	gEBI = "getElementById",
	pN = "parentNode",
	appContentId = "app-content",
	appContent = d[gEBI](appContentId) || "",
	appContentParent = appContent[pN] || "",
	loadVirtualPage = function (c, h, f) {
		if (c && h) {
			LoadingSpinner.show();
			insertExternalHTML(c, h, f);
		}
	},
	reinitVirtualPage = function (titleString) {
		titleString = titleString || "";
		/*!
		 * hide loading spinner before scrolling
		 */
		d.title = (titleString ? titleString +  " - "  : "" ) + (initialDocumentTitle ? initialDocumentTitle + (userBrowsingDetails ? userBrowsingDetails : "") : "");
		manageContentsSelect();
		manageExternalLinkAll(appContentParent);
		manageChaptersSelect(appContentParent);
		manageExpandingLayers(appContentParent);
		initMasonry(appContentParent);
		handleDataSrcImageAll();
		LoadingSpinner.hide(scroll2Top.bind(null, 0, 20000));
	},
	loadNotFoundPage = function (containerClass) {
		var container = d[gEBI](containerClass) || "",
		msgText = d.createRange().createContextualFragment('<div class="content-wrapper"><div class="grid-narrow grid-pad"><div class="col col-1-1"><div class="content"><p>Нет такой страницы. <a href="#/contents">Исправить?</a></p></div></div></div></div>');
		if (container) {
			LoadingSpinner.show();
			removeChildren(container);
			appendFragment(msgText, container);
			reinitVirtualPage("Нет такой страницы");
		}
	};
	/*!
	 * init routie
	 * "#" => ""
	 * "#/" => "/"
	 * "#/home" => "/home"
	 */
	if (appContent) {
		/* console.log("triggered function: routie"); */
		routie({
			"": function () {
				loadVirtualPage(appContentId, "./pages/contents.html", function () {
					reinitVirtualPage("Содержание");
				});
			},
			"/contents": function () {
				loadVirtualPage(appContentId, "./pages/contents.html", function () {
					reinitVirtualPage("Содержание");
				});
			},
			"/aids_most_commonly_used_idioms": function () {
				loadVirtualPage(appContentId, "./pages/aids/aids_most_commonly_used_idioms.html", function () {
					reinitVirtualPage("Пособия - Most Commonly Used Idioms");
				});
			},
			"/aids_topics": function () {
				loadVirtualPage(appContentId, "./pages/aids/aids_topics.html", function () {
					reinitVirtualPage("Пособия - Топики на английском");
				});
			},
			"/articles_reading_rules_utf": function () {
				loadVirtualPage(appContentId, "./pages/articles/articles_reading_rules_utf.html", function () {
					reinitVirtualPage("Статьи - Правила чтения");
				});
			},
			"/tests_advanced_grammar_in_use_level_test": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_advanced_grammar_in_use_level_test.html", function () {
					reinitVirtualPage("Тесты - Advanced Grammar in Use Level Test");
				});
			},
			"/tests_english_grammar_in_use_level_test": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_english_grammar_in_use_level_test.html", function () {
					reinitVirtualPage("Тесты - English Grammar in Use Level Test");
				});
			},
			"/tests_essential_grammar_in_use_level_test": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_essential_grammar_in_use_level_test.html", function () {
					reinitVirtualPage("Тесты - Essential Grammar in Use Level Test");
				});
			},
			"/tests_ege_english_test_sample": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_ege_english_test_sample.html", function () {
					reinitVirtualPage("Тесты - Демо-вариант ЕГЭ-11 АЯ (ПЧ)");
				});
			},
			"/tests_ege_english_test_sample_speaking": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_ege_english_test_sample_speaking.html", function () {
					reinitVirtualPage("Тесты - Демо-вариант ЕГЭ-11 АЯ (УЧ)");
				});
			},
			"/tests_ege_essay_sample": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_ege_essay_sample.html", function () {
					reinitVirtualPage("Тесты - ЕГЭ: Задание 40");
				});
			},
			"/tests_gia_ege_letter_sample": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_gia_ege_letter_sample.html", function () {
					reinitVirtualPage("Тесты - ГИА / ЕГЭ: Задания 33, 39, 40");
				});
			},
			"/tests_gia_english_test_sample": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_gia_english_test_sample.html", function () {
					reinitVirtualPage("Тесты - Демо-вариант ГИА-9 (ОГЭ) АЯ (ПЧ)");
				});
			},
			"/tests_gia_english_test_sample_speaking": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_gia_english_test_sample_speaking.html", function () {
					reinitVirtualPage("Тесты - Демо-вариант ГИА-9 (ОГЭ) АЯ (УЧ)");
				});
			},
			"/tests_languagelink_online_test": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_languagelink_online_test.html", function () {
					reinitVirtualPage("Тесты - Уровневый тест");
				});
			},
			"/tests_common_mistakes_test_advanced": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_common_mistakes_test_advanced.html", function () {
					reinitVirtualPage("Тесты - Common Mistakes Test Advanced");
				});
			},
			"/tests_english_allrussian_olympiad_regional_stage_2013": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_english_allrussian_olympiad_regional_stage_2013.html", function () {
					reinitVirtualPage("Тесты - XIV Олимпиада по английскому");
				});
			},
			"/tests_grammar_tests_with_answers": function () {
				loadVirtualPage(appContentId, "./pages/tests/tests_grammar_tests_with_answers.html", function () {
					reinitVirtualPage("Тесты - Тесты по грамматике");
				});
			},
			"/grammar_all_whole_entire": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_all_whole_entire.html", function () {
					reinitVirtualPage("Грамматика - All / the whole / the entire");
				});
			},
			"/grammar_alone_by_myself_and_on_my_own": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_alone_by_myself_and_on_my_own.html", function () {
					reinitVirtualPage("Грамматика - Alone / by myself / on my own");
				});
			},
			"/grammar_attributes_order": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_attributes_order.html", function () {
					reinitVirtualPage("Грамматика - Порядок определений");
				});
			},
			"/grammar_can_could_be_able_to": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_can_could_be_able_to.html", function () {
					reinitVirtualPage("Грамматика - Can / could / be able to");
				});
			},
			"/grammar_capital_letters": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_capital_letters.html", function () {
					reinitVirtualPage("Грамматика - С большой или с маленькой");
				});
			},
			"/grammar_comma_before_who_which_that": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_comma_before_who_which_that.html", function () {
					reinitVirtualPage("Грамматика - Who / which / that и запятые");
				});
			},
			"/grammar_common_and_proper_nouns": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_common_and_proper_nouns.html", function () {
					reinitVirtualPage("Грамматика - Нарицательное / собственное");
				});
			},
			"/grammar_conditionals": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_conditionals.html", function () {
					reinitVirtualPage("Грамматика - Согласование времен");
				});
			},
			"/grammar_degrees_of_comparison": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_degrees_of_comparison.html", function () {
					reinitVirtualPage("Грамматика - Степени сравнения");
				});
			},
			"/grammar_ex_former": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_ex_former.html", function () {
					reinitVirtualPage("Грамматика - «Ex-» и «former», или Осторожно: морпехи");
				});
			},
			"/grammar_foreign_words": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_foreign_words.html", function () {
					reinitVirtualPage("Грамматика - Заимствованные слова");
				});
			},
			"/grammar_glossary_of_grammatical_terms": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_glossary_of_grammatical_terms.html", function () {
					reinitVirtualPage("Грамматика - Glossary of Grammatical Terms");
				});
			},
			"/grammar_grammar_girl_s_quick_and_dirty_grammar_at_a_glance": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_grammar_girl_s_quick_and_dirty_grammar_at_a_glance.html", function () {
					reinitVirtualPage("Грамматика - Quick and Dirty Grammar");
				});
			},
			"/grammar_in_at_on": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_in_at_on.html", function () {
					reinitVirtualPage("Грамматика - In / at / on");
				});
			},
			"/grammar_in_hospital_at_work": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_in_hospital_at_work.html", function () {
					reinitVirtualPage("Грамматика - In hospital / at work etc.");
				});
			},
			"/grammar_irregular_verbs": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_irregular_verbs.html", function () {
					reinitVirtualPage("Грамматика - Неправильные глаголы (таблица)");
				});
			},
			"/grammar_may_might_be_allowed_to": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_may_might_be_allowed_to.html", function () {
					reinitVirtualPage("Грамматика - May / might / be allowed to");
				});
			},
			"/grammar_modal_verbs": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_modal_verbs.html", function () {
					reinitVirtualPage("Грамматика - Модальные глаголы");
				});
			},
			"/grammar_much_many_little_few": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_much_many_little_few.html", function () {
					reinitVirtualPage("Грамматика - Much / many / little / few");
				});
			},
			"/grammar_or_nor_neither": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_or_nor_neither.html", function () {
					reinitVirtualPage("Грамматика - Or / nor / neither");
				});
			},
			"/grammar_phrasal_verbs": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_phrasal_verbs.html", function () {
					reinitVirtualPage("Грамматика - Фразовые глаголы");
				});
			},
			"/grammar_reported_speech": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_reported_speech.html", function () {
					reinitVirtualPage("Грамматика - Косвенная речь");
				});
			},
			"/grammar_saying_numbers": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_saying_numbers.html", function () {
					reinitVirtualPage("Грамматика - Числа по-английски");
				});
			},
			"/grammar_stative_and_action_verbs": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_stative_and_action_verbs.html", function () {
					reinitVirtualPage("Грамматика - Неконтиниусные глаголы");
				});
			},
			"/grammar_the_listing_comma": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_the_listing_comma.html", function () {
					reinitVirtualPage("Грамматика - Серийная запятая");
				});
			},
			"/grammar_too_enough_so_such": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_too_enough_so_such.html", function () {
					reinitVirtualPage("Грамматика - Too / enough и so / such");
				});
			},
			"/grammar_to_me_for_me": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_to_me_for_me.html", function () {
					reinitVirtualPage("Грамматика - To me / for me, или Надуманная проблема");
				});
			},
			"/grammar_translating_participles": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_translating_participles.html", function () {
					reinitVirtualPage("Грамматика - Перевод причастий и деепричастий");
				});
			},
			"/grammar_usage_of_articles_a_the": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_usage_of_articles_a_the.html", function () {
					reinitVirtualPage("Грамматика - Артикли a / an и the");
				});
			},
			"/grammar_usage_of_hyphens": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_usage_of_hyphens.html", function () {
					reinitVirtualPage("Грамматика - Расстановка дефисов");
				});
			},
			"/grammar_usage_of_tenses": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_usage_of_tenses.html", function () {
					reinitVirtualPage("Грамматика - Употребление времен");
				});
			},
			"/grammar_when_must_means_probably": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_when_must_means_probably.html", function () {
					reinitVirtualPage("Грамматика - Когда must = «должно быть»");
				});
			},
			"/grammar_word_order": function () {
				loadVirtualPage(appContentId, "./pages/grammar/grammar_word_order.html", function () {
					reinitVirtualPage("Грамматика - Порядок слов в предложении");
				});
			},
			"/reading_russia_ukraine_war_conflict_vocabulary": function () {
				loadVirtualPage(appContentId, "./pages/reading/reading_russia_ukraine_war_conflict_vocabulary.html", function () {
					reinitVirtualPage("Чтение - Russia-Ukraine War Conflict");
				});
			},
			"/reading_the_man_with_the_scar": function () {
				loadVirtualPage(appContentId, "./pages/reading/reading_the_man_with_the_scar.html", function () {
					reinitVirtualPage("Чтение - The Man with the Scar");
				});
			},
			"/transcripts_linguaspectrum_essential_british_english_expressions": function () {
				loadVirtualPage(appContentId, "./pages/transcripts/transcripts_linguaspectrum_essential_british_english_expressions.html", function () {
					reinitVirtualPage("Транскрипты - Essential British English Expressions");
				});
			},
			"/transcripts_linguaspectrum_spell": function () {
				loadVirtualPage(appContentId, "./pages/transcripts/transcripts_linguaspectrum_spell.html", function () {
					reinitVirtualPage("Транскрипты - 44 Most Often Misspelled Words");
				});
			},
			"/transcripts_video_vocab_transcripts": function () {
				loadVirtualPage(appContentId, "./pages/transcripts/transcripts_video_vocab_transcripts.html", function () {
					reinitVirtualPage("Транскрипты - English Vocabulary for Business");
				});
			},
			"/webdev_about": function () {
				loadVirtualPage(appContentId, "./pages/webdev/webdev_about.html", function () {
					reinitVirtualPage("О приложении «Английский без регистрации»");
				});
			},
			"/*": function () {
				loadNotFoundPage(appContentId);
			}
		});
	}
};
document.ready().then(initRoutie);
/*!
 * observe mutations
 * bind functions only for inserted DOM
 * @param {String} ctx HTML Element class or id string
 */
/* var observeMutations = function (ctx) {
	"use strict";
	ctx = ctx && ctx.nodeName ? ctx : "";
	var getMutations = function (e) {
		var triggerOnMutation = function (m) {
			console.log("mutations observer: " + m.type);
			console.log(m.type, "target: " + m.target.tagName + ("." + m.target.className || "#" + m.target.id || ""));
			console.log(m.type, "added: " + m.addedNodes.length + " nodes");
			console.log(m.type, "removed: " + m.removedNodes.length + " nodes");
			if ("childList" === m.type || "subtree" === m.type) {
				mo.disconnect();
			}
		};
		for (var i = 0, l = e.length; i < l; i += 1) {
			triggerOnMutation(e[i]);
		}
		forEach(e, triggerOnMutation);
	};
	if (ctx) {
		var mo = new MutationObserver(getMutations);
		mo.observe(ctx, {
			childList: !0,
			subtree: !0,
			attributes: !1,
			characterData: !1
		});
	}
}; */
/*!
 * apply changes to inserted DOM
 * because replace child is used in the first place
 * to insert new content, and if parent node doesnt exist
 * inner html method is applied,
 * the parent node should be observed, not the target
 * node for the insertion
 */
/* var updateInsertedDom = function () {
	"use strict";
	var w = globalRoot,
	d = document,
	gEBI = "getElementById",
	pN = "parentNode",
	ctx = d[gEBI]("app-content")[pN] || "",
	locationHash = w.location.hash || "";
	if (ctx && locationHash) {
		console.log("triggered function: updateInsertedDom");
		observeMutations(ctx);
	}
};
globalRoot.addEventListener("hashchange", updateInsertedDom); */
/*!
 * init manUP.js
 */
/* var initManUp = function () {
	"use strict";
	if ("undefined" !== typeof getHTTP && getHTTP()) {
		var jsUrl = "/cdn/ManUp.js/0.7/js/manup.fixed.min.js";
		if (!scriptIsLoaded(jsUrl)) {
			loadJS(jsUrl);
		}
	}
};
document.ready().then(initManUp); */
/*!
 * show page, finish ToProgress
 */
var showPageFinishProgress = function () {
	"use strict";
	var d = document,
	b = d.body || "",
	gEBI = "getElementById",
	gEBCN = "getElementsByClassName",
	container = d[gEBI]("container") || "",
	holder = d[gEBCN]("holder-site-logo")[0] || "",
	showPage = function () {
		setStyleOpacity(container, 1);
	};
	if (container) {
		/* if ("undefined" !== typeof imagesPreloaded) {
			var timers = new Timers();
			timers.interval(function () {
				if ("undefined" !== typeof imagesPreloaded && imagesPreloaded) {
					timers.clear();
					timers = null;
					showPage();
				}
			}, 100);
		} else { */
			showPage();
		/* } */
	}
	if (holder) {
		var timers2 = new Timers();
		timers2.timeout(function () {
			timers2.clear();
			timers2 = null;
			holder.classList.add("animated", "fadeOut");
			var timers3 = new Timers();
			timers3.timeout(function () {
				timers3.clear();
				timers3 = null;
				setStyleDisplayNone(holder);
				/* setStyleZindex(holder, "-1"); */
				if (b) {
					b.style.overflowY = "auto";
				}
			}, 500);
		}, 1500);
	}
};
document.ready().then(showPageFinishProgress);
