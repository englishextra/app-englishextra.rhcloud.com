# app-englishextra.rhcloud.com

*The online demo of app-englishextra*

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/79258b120f6642a6a05eaa7ca3ccdb41)](https://www.codacy.com/app/englishextra/app-englishextra-rhcloud-com?utm_source=github.com&utm_medium=referral&utm_content=englishextra/app-englishextra.rhcloud.com&utm_campaign=badger)
[![Travis](https://img.shields.io/travis/englishextra/app-englishextra.rhcloud.com.svg)](https://github.com/englishextra/app-englishextra.rhcloud.com)

## On-line

 - [the website](https://app-englishextra.rhcloud.com/)

## Dashboard

<https://openshift.redhat.com/app/console/application/57964d8589f5cfc1a2000061-spa>

## Production Push URL

```
ssh://57964d8589f5cfc1a2000061@app-englishextra.rhcloud.com/~/git/app.git/
```

## Remotes

 - [GitHub](https://github.com/englishextra/app-englishextra.rhcloud.com)
 - [BitBucket](https://bitbucket.org/englishextra/app-englishextra.rhcloud.com)
 - [GitLab](https://gitlab.com/englishextra/app-englishextra.rhcloud.com)

## Copyright

© [englishextra.github.com](https://englishextra.github.com/), 2015-2017