/*jslint node: true */
/*jslint esversion: 6 */
module.exports = {
	entry : "./diy/libs/app-englishextra/js/bundle.js",
	output : {
		path : "./diy/libs/app-englishextra/js",
		filename : "bundle.min.js"
	}
};
